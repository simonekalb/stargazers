## Stargazer Challenge App
This is a simple app made in Swift. Its purpose it to show some of the stargazers of a GitHub Repo given the repo name and its owner.
### How to run 
Just open the project file in Xcode and press run. The application is build on Xcode version 12.5 and can run on devices from iOS 10.0 or above, just by changing the deployment target in Xcode. 
## Architectural choices
I decided to build this app with vanilla Swift without external libraries or new framework such as Combine (iOS 13+) or SwiftUI (iOS 14+). If you find this choice masochistic, well, it is. I had to write a lot of boilerplate code and wasted a lot of time doing this...My goal was to demostrate I can work with legacy code (even in Objective-C if needed) as well with newest frameworks. I made a mixed approach with some views made by writing constraints programmatically, and using the classic Interface Builder approach for the views you don't need to change very often. The pattern I used is a basic MVVC (here as well no RXSwift) in which the ViewModel is performing the network call and holds the business logic of the app.

The Network layer is a simple shared Instance so the testing part can be really easy to implement and hopefully can be reused in other project just by abstracting some of the logic coupled with GitHub APIs. Actually I would like to extend it in order to group the download image calls with a dispatch group to limit the number of threads that perform nasynchronous tasks.

The first view controller is tighly coupled with ViewModel, I would like to loose the coupling by using a dependency container and a dependecy injection fw with [Dip](https://github.com/AliSoftware/Dip), but I didn't just for a matter of time. Dip could handle the injection of the viewController I made in the ViewControllerFactory in order to inject a view controller of your choice into the navigation controller in order to get a much more controllable navigation flow for some reason (think about deeplink navigation or custom navigation flow based on user status if logged or not and so on).

Another interesting topic should be the generalization of the table view controller: now we got a really specialized one but think about for a second if you can pass just a Cell and an Item and define just a callback to configure the cell as in the next code (made in playground):
```swift 
import Foundation
import UIKit
import PlaygroundSupport

protocol Item: Decodable {}

struct Episodes: Item {
    
    var description: String
    var episodeURL: String
    var id: Int
}


final class EpisodeCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

struct Season: Item {
    
    var description: String
    var number: Int
}

final class SeasonCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

struct MockData {

    static let mockEpisodes01 = Episodes(description: "S01E0 - Pilot", episodeURL: "https://simonekalb.me", id: 1)
    
    static let mockEpisodes02 = Episodes(description: "S01E1 - UITableView Basics", episodeURL: "https://simonekalb.me", id: 2)

    static let mockEpisodes03 = Episodes(description: "S01E2 - UITableViewCell Basics", episodeURL: "https://simonekalb.me", id: 3)
    
    static let mockEpisodes04 = Episodes(description: "S01E4 - UICollectionView Basics", episodeURL: "https://simonekalb.me", id: 4)
    
    static let fakeEpisodes = [mockEpisodes01, mockEpisodes02, mockEpisodes03, mockEpisodes04]
    
    static let mockSeasonOne = Season(description: "First Season", number: 1)
    
    static let mockSeasonTwo = Season(description: "Second Season", number: 2)
    
    static let fakeSeason = [mockSeasonOne, mockSeasonTwo]
}


final class ItemViewController<Item, Cell: UITableViewCell>: UITableViewController {
    
    var items: [Item] = []
    let reuseIdentifier = "Cell"
    let configure: (Cell, Item) -> ()
    var didSelect: (Item) -> () = {_ in }
    
    init(items: [Item],
         configure: @escaping (Cell, Item)-> ()) {
            self.configure = configure
            super.init(style: .plain)
            self.items = items
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        tableView.register(Cell.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        didSelect(item)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! Cell
        let item = self.items[indexPath.row]
        configure(cell, item)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
}

let episodeVC = ItemViewController(items: MockData.fakeEpisodes, configure: { (cell: EpisodeCell, episode) in
    cell.textLabel?.text = "\(episode.id) " + episode.description
    })
episodeVC.title = "Episodes"

let seasonVC = ItemViewController(items: MockData.fakeSeason) { (cell: SeasonCell, season) in
    cell.textLabel?.text = season.description
    cell.detailTextLabel?.text = "\(season.number)"
}
seasonVC.title = "Seasons"

//: Navigation Controller
let nc = UINavigationController(rootViewController: seasonVC)
nc.view.frame = CGRect(x: 0, y: 0, width: 320, height: 480)

seasonVC.didSelect = { episode in
    nc.pushViewController(episodeVC, animated: true)
}

PlaygroundPage.current.liveView = nc.view
```
Cool isn't?
There are plenty of topic to talk about even in this little app: RXSwift to implement an infinite scrolling, testability and protocol oriented programming and so on and so forth. 
Given the time I had to make some decision, probably some are not correct and need a refactoring.

## Coding style and branching model
Spaces over tabs ftw, I used a Gitflow branching model in which I committed very frequently, --no-ff when merging into develop. 

## What I'm proud of 
I did it, and I learned a lot about my coding style in the process, it was a funny experience.

## What I'm not proud of
Some decisions I made were incompatible with the time span I had at my disposal. 

## Did you comment the code?
No. At all, in this case the code should be self explanatory.

### No CocoaPods nor Carthage, SPM
I didn't use any of this package managers because for this app I really don't needed, probably the best choice for a MVVM paradigm should be using RXSwift via CocoaPod for example.

#### Feel free to chat and talk about the implementation 
I'm willing to discuss all my implementation decisions, so feel free to drop me an email at [simone.kalb@gmail.com](mailto: simone.kalb@gmail.com) 

