//
//  StargazerViewModel.swift
//  stargazer
//
//  Created by Simone Kalb on 30/08/21.
//

import Foundation

final class StargazerViewModel: NSObject  {
    
    public var items : [Stargazer] = []
    
    func getStargazers(for repo:String, owner:String, completed: @escaping(_ items: [Stargazer]?, _ alert: Alert?)-> Void)  {
        
        NetworkManager.shared.getStargazers(owner: owner, repo: repo) { [self] result in
            DispatchQueue.main.async {
                switch result {
                    case .success(let stargazers):
                        self.items = stargazers ?? []
                        completed(self.items, nil)
                    case .failure(let error):
                        switch error {
                            case .malformedURL:
                                completed(nil, AlertContext.malformedURL)
                            case .networkingError:
                                completed(nil, AlertContext.networkingError)
                            case .apiResponseError:
                                completed(nil, AlertContext.apiResponseError)
                            case .unexpectedReponseCode:
                                completed(nil, AlertContext.unexpectedReponseCode)
                            case .parsingError:
                                completed(nil, AlertContext.parsingError)
                            case .responseDataError:
                                completed(nil, AlertContext.responseDataError)
                        }
                }
            }
        }
    }
}
