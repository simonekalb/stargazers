//
//  ViewController.swift
//  GHStargazer
//
//  Created by Simone Kalb on 30/08/21.
//

import UIKit

class StargazersViewController: UITableViewController {
    
    var viewModel: StargazerViewModel
    let reuseIdentifier = "Cell"
    let viewTitle = "Stargazers"
    
    init(viewModel: StargazerViewModel) {
        self.viewModel = viewModel
        super.init(style: .insetGrouped)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewTitle
        registerCell()
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension StargazersViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! StargazerViewCell
        let item = self.viewModel.items[indexPath.row]
        cell.loginLabel.text = item.login
        cell.avatarImage.loadImage(from: item.avatar_url)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    private func registerCell() {
        tableView.register(StargazerViewCell.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
}
