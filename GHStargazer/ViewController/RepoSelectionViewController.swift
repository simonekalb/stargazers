//
//  RepoSelectionViewController.swift
//  GHStargazer
//
//  Created by Simone Kalb on 03/09/21.
//

import UIKit

class RepoSelectionViewController: UIViewController {

    @IBOutlet var onBoardingText: UILabel!
    @IBOutlet var repoSelectionText: UILabel!
    @IBOutlet var ownerSelectionText: UILabel!
    @IBOutlet var repoTextInput: UITextField!
    @IBOutlet var ownerTextInput: UITextField!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    
    var viewModel = StargazerViewModel()
    var alertView: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.repoTextInput.delegate = self
        self.ownerTextInput.delegate = self
        
        // UI Setup
        setTitle()
        setupOnBoardingText()
        setupInputLabelText()
        setupSearchButton()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.repoTextInput.text = ""
        self.ownerTextInput.text = ""
    }
}


// MARK: UI Setup
extension RepoSelectionViewController {
    
    private func setupOnBoardingText() {
        onBoardingText.text = getOnBoardingText()
        
    }
    private func setupInputLabelText() {
        repoSelectionText.text = getRepoSelectionText()
        ownerSelectionText.text = getRepoOwnerText()
    }
    
    private func setupSearchButton() {
        searchButton.setAttributedTitle(getButtonText(), for: .normal)
    }
    
    private func setTitle() {
        title = getTitle()
    }
}

// MARK: Behaviour
extension RepoSelectionViewController {
    
    @IBAction func buttonTapped(sender: UIButton?) {
        guard let repo = self.repoTextInput.text?.trimmingCharacters(in: .whitespaces) ,
              let owner = self.ownerTextInput.text?.trimmingCharacters(in: .whitespaces) else {
            return
        }
        
        if(repo.isEmpty || owner.isEmpty) {
            self.alertView = UIAlertController(title: AlertContext.wrongInput.title, message: AlertContext.wrongInput.message, preferredStyle: .alert)
            self.alertView!.addAction((UIAlertAction(title: AlertContext.wrongInput.buttonText, style: .destructive, handler: nil)))
            self.present(self.alertView!, animated: true, completion: nil)
            
        }
        
        fetchData(for: repo, owner: owner)
    }
    
    private func fetchData(for repo: String, owner: String) {
        
        self.viewModel.getStargazers(for: repo, owner: owner, completed: { [weak self] items, alert in
            guard let strongSelf = self else {
                return
            }
            if items != nil {
                strongSelf.navigationController?.pushViewController(ViewControllerFactory.getDetailViewController(viewModel: strongSelf.viewModel), animated: true)
                return
            }
            if let alert = alert {
                strongSelf.alertView = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .alert)
                strongSelf.alertView!.addAction((UIAlertAction(title: alert.buttonText, style: .destructive, handler: nil)))
                strongSelf.present(strongSelf.alertView!, animated: true, completion: nil)
            }
            
        })
    }
}

// MARK: Static Content Setup
extension RepoSelectionViewController {
    
    private func getOnBoardingText() -> String {
        return "Welcome to the **Stargazer** app.\n Please insert on the following fields the owner of the repo you want to show the stargazers to and the repo name itself.\n For example if you want to look for Apple Swift stagazers insert apple in the owner field and swift in the repo name field."
        // return NSLocalizedString("repo.selection.onboarding.text", comment: nil)
    }
    
    private func getRepoSelectionText() -> String {
        return "Name of the repository"
        // return NSLocalizedString("repo.selection.repo.input.label.text", comment: nil)
    }
    
    private func getRepoOwnerText() -> String {
        return "Name of the owner"
        // return NSLocalizedString("repo.selection.owner.input.label.text", comment: nil)
    }
    
    private func getButtonText() -> NSAttributedString {
        return NSAttributedString(string: "Search")
        // return NSAttributedString(string: NSLocalizedString("repo.selection.search.button.text", comment: nil))
    }
    
    private func getTitle() -> String {
        return "🧑‍💻 Search"
        // return NSLocalizedString("repo.selection.title.text", comment: nil)
    }
    
    
}

// MARK: UITextFieldDelegate
extension RepoSelectionViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        let pointInTable:CGPoint = textField.superview!.convert(textField.frame.origin, to:self.repoTextInput)
        var contentOffset:CGPoint = self.scrollView.contentOffset
        contentOffset.y  = pointInTable.y
        self.scrollView.setContentOffset(contentOffset, animated: true)
    }
}
