//
//  APIError.swift
//  stargazer
//
//  Created by Simone Kalb on 30/08/21.
//

import Foundation

enum APIError : Error {
    case networkingError
    case malformedURL
    case apiResponseError
    case unexpectedReponseCode
    case responseDataError
    case parsingError
}
