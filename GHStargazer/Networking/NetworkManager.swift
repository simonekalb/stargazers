//
//  NetworkManager.swift
//  stargazer
//
//  Created by Simone Kalb on 30/08/21.
//

import Foundation
import UIKit


class NetworkManager: NSObject {
    
    static let shared = NetworkManager()
    private let cache = NSCache<NSString, UIImage>()
    
    static let baseUrl = "https://api.github.com/"
    
    private override init() {}
    
    func getStargazers(owner: String, repo: String, per_page: Int = 100, page: Int = 1, completed: @escaping (Result<[Stargazer]?, APIError>)-> Void) {
        let stargazerUrl = NetworkManager.baseUrl + "repos/\(owner)/\(repo)/stargazers?per_page=\(per_page)&page=\(page)"
        
        guard let url = URL(string: stargazerUrl) else {
            completed(.failure(.malformedURL))
            return
        }
        
        let task = URLSession.shared.dataTask(with: getRequest(from: url)) { data, response, error in
            
            if let _ = error {
                completed(.failure(.apiResponseError))
                return
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completed(.failure(.unexpectedReponseCode))
                return
            }

            guard let data = data else {
                completed(.failure(.responseDataError))
                return
            }

            do {
                let decoder = JSONDecoder()
                let stargazers = try decoder.decode([Stargazer].self, from: data)
                completed(.success(stargazers))
            } catch {
                completed(.failure(.parsingError))
            }

        }
        task.resume()
    }
    
    func downloadImage(from urlString: String, completed: @escaping(UIImage?) -> Void) {

        let cacheKey = NSString(string: urlString)

        if let image = cache.object(forKey: cacheKey) {
            completed(image)
            return
        }

        guard let url = URL(string: urlString) else {
            completed(nil)
            return
        }

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, let image = UIImage(data: data) else {
                completed(nil)
                return
            }
            self.cache.setObject(image, forKey: cacheKey)
            completed(image)
        }

        task.resume()
    }
}

extension NetworkManager {
    
    static let httpMethod = "GET"
    static let acceptHeaderValue = "application/vnd.github.v3+json"
    
    func getRequest(from url: URL) -> URLRequest {
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = NetworkManager.httpMethod
        request.addValue(NetworkManager.acceptHeaderValue, forHTTPHeaderField: "Accept")
        return request as URLRequest
    }
    
}
