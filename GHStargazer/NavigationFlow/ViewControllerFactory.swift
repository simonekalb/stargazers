//
//  InitialViewControllerFactory.swift
//  stargazer
//
//  Created by Simone Kalb on 30/08/21.
//

import Foundation
import UIKit


struct ViewControllerFactory {
    
    static func getDetailViewController(viewModel: StargazerViewModel) -> StargazersViewController {
         return StargazersViewController(viewModel: viewModel)
    }
    
    static func getInitialViewController() -> RepoSelectionViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RepoSelectionViewController") as! RepoSelectionViewController
    }
}
