//
//  AppDelegate.swift
//  GHStargazer
//
//  Created by Simone Kalb on 30/08/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setup()
        return true
    }
}

extension AppDelegate {
    private func setup() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let nc = UINavigationController(rootViewController: ViewControllerFactory.getInitialViewController())
        self.window!.rootViewController = nc
        self.window?.makeKeyAndVisible()
    }
}

