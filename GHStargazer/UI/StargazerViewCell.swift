//
//  StargazerViewCell.swift
//  stargazer
//
//  Created by Simone Kalb on 30/08/21.
//

import Foundation
import UIKit

final class StargazerViewCell: UITableViewCell {
    
    var avatarImage =  ImageLoader(image: nil)
    var loginLabel =  UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(avatarImage)
        addSubview(loginLabel)
        
        configureLoginLabel()
        configureAvatarImage()
        
        setAvatarImageConstraints()
        setLoginLabelConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: UI Configuration
extension StargazerViewCell {
    
    func configureAvatarImage() {
        avatarImage.layer.cornerRadius = 12
        avatarImage.clipsToBounds = true
    }
    
    func configureLoginLabel() {
        loginLabel.numberOfLines = 0
        loginLabel.adjustsFontSizeToFitWidth = true
    }
    
    func setAvatarImageConstraints() {
        avatarImage.translatesAutoresizingMaskIntoConstraints = false
        avatarImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        avatarImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        avatarImage.heightAnchor.constraint(equalToConstant: 60).isActive = true
        avatarImage.widthAnchor.constraint(equalTo: avatarImage.heightAnchor).isActive = true
    }
    
    func setLoginLabelConstraints() {
        loginLabel.translatesAutoresizingMaskIntoConstraints = false
        loginLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        loginLabel.leadingAnchor.constraint(equalTo: avatarImage.trailingAnchor, constant: 20).isActive = true
        loginLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        loginLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
    }
}
