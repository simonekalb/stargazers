//
//  ImageLoader.swift
//  GHStargazer
//
//  Created by Simone Kalb on 02/09/21.
//

import Foundation
import UIKit

final class ImageLoader: UIImageView {
    
    var urlString: String = ""
    
    override init(image: UIImage?) {
        guard let image = image else {
            super.init(image: UIImage(named: "profile"))
            return
        }
        super.init(image: image)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadImage(from urlString: String) {
        NetworkManager.shared.downloadImage(from: urlString) { uiImage in
            guard let uiImage = uiImage else { return }
            DispatchQueue.main.async {
                self.image = uiImage
            }
        }
    }
    
}
