//
//  Alert.swift
//  GHStargazer
//
//  Created by Simone Kalb on 01/09/21.
//

import Foundation
import UIKit

struct Alert {
    
    var message: String
    var title: String
    var buttonText: String
    
}
