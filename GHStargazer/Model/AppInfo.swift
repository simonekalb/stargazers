//
//  AppInfo.swift
//  GHStargazer
//
//  Created by Simone Kalb on 02/09/21.
//

import Foundation

struct AppInfo {
    static let appDeveloper = "Simone Kalb"
    static let appDeveloperContact = "simone.kalb@gmail.com"
    static let appVersion = "0.1"
}
