//
//  Stargazer.swift
//  stargazer
//
//  Created by Simone Kalb on 30/08/21.
//

import Foundation

/*
 Example response
 {
     "login": "allanmaio",
     "id": 345807,
     "node_id": "MDQ6VXNlcjM0NTgwNw==",
     "avatar_url": "https://avatars.githubusercontent.com/u/345807?v=4",
     "gravatar_id": "",
     "url": "https://api.github.com/users/allanmaio",
     "html_url": "https://github.com/allanmaio",
     "followers_url": "https://api.github.com/users/allanmaio/followers",
     "following_url": "https://api.github.com/users/allanmaio/following{/other_user}",
     "gists_url": "https://api.github.com/users/allanmaio/gists{/gist_id}",
     "starred_url": "https://api.github.com/users/allanmaio/starred{/owner}{/repo}",
     "subscriptions_url": "https://api.github.com/users/allanmaio/subscriptions",
     "organizations_url": "https://api.github.com/users/allanmaio/orgs",
     "repos_url": "https://api.github.com/users/allanmaio/repos",
     "events_url": "https://api.github.com/users/allanmaio/events{/privacy}",
     "received_events_url": "https://api.github.com/users/allanmaio/received_events",
     "type": "User",
     "site_admin": false
   }
 */

struct Stargazer: Codable {

    var login: String
    var id: Int
    var node_id: String
    var avatar_url: String
}

struct MockData {

    static let stargazer = [sampleStargazer,sampleStargazer,sampleStargazer]
    static let sampleStargazer = Stargazer(login: "allanmaio",
                                           id: 345807,
                                           node_id: "MDQ6VXNlcjM0NTgwNw==",
                                           avatar_url: "https://avatars.githubusercontent.com/u/345807?v=4"
                                           )
}
