//
//  AlertContext.swift
//  GHStargazer
//
//  Created by Simone Kalb on 02/09/21.
//

import Foundation

struct AlertContext {

    static let networkingError: Alert = Alert(message: "A networking error has encountered, during the fetch of required information. Check your connection status and try again by refreshing the view.",title: "Networking Error", buttonText: "Ok, thanks.")
    static let malformedURL: Alert = Alert(message: "The URL you're trying to request is not valid, maybe the user/repository combination is not valid. Try another combo.",title: "Malformed URL", buttonText: "Ok, thanks.")
    static let apiResponseError: Alert = Alert(message: "The requested URL responded with a wrong data. Try to contact app developer support at: \(AppInfo.appDeveloperContact)", title: "URL wrong data", buttonText: "Ok, thanks.")
    static let unexpectedReponseCode: Alert = Alert(message: "The GitHub API server replied with a wrong response code. Try to contact app developer support at: \(AppInfo.appDeveloperContact) ", title: "Wrong GitHub reponse", buttonText: "Ok, thanks.")
    static let responseDataError: Alert = Alert(message: "The GitHub API server replied with a wrong data. Try to contact app developer support at: \(AppInfo.appDeveloperContact) ", title: "Wrong GitHub data", buttonText: "Ok, thanks.")
    static let parsingError: Alert = Alert(message: "The app is not able to decode data from server. Try to contact app developer support at: \(AppInfo.appDeveloperContact) ", title: "Unable to decode data", buttonText: "Ok, thanks.")
    static let wrongInput: Alert = Alert(message: "Neither the repo nor the owner should be left empty.Please fill the text views", title: "Fill the text views", buttonText: "Ok, thanks.")
    
}
